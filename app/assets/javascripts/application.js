// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require_tree .


$(document).ready(function() {
    $(".activate-account").bind('change', function(){

        $.ajax({
            url: '/users/'+this.name+'/update',
            type: 'POST',
            data: {"completed": this.checked},
            dataType: 'html',
            error: function(xhr, status, error) {console.log({xhr: xhr, error: error, status: status})}
        }).done(function (response){
            $('main.container').append(response);

            var $snackbar = $('.snackbar');
            $('.snackbar-action button').click(function() {
                $snackbar.remove();
            });

            setTimeout(function () {
                $snackbar.remove();
            }, 5000);
        });
    });


});