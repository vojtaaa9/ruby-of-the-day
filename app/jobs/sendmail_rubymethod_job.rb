class SendmailRubymethodJob < ApplicationJob
  queue_as :default

  def self.run
    users = User.where(is_activated: true).where.not(confirmed_at: nil)
    users.each do |user|
      user.generate_new_ruby_method
      UserMailer.ruby_method_of_the_day(user).deliver_now
    end

    return users
  end
end
