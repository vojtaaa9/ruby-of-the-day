class UsersController < ApplicationController
  before_action :is_activated?, except: :account_locked
  before_action :authenticate_user!, only: [:destroy, :index, :profile, :update]

  layout false, only: :update

  def home
    return redirect_to user_profile_path(user_id: current_user.id) if user_signed_in?

    render 'home'
  end

  def account_locked
    render 'account_locked'
  end

  def index
    if user_signed_in? && current_user.is_admin?
      @users = User.order(:name).page(params[:page]).decorate
      render 'list_user'
    else
      redirect_to root_path
    end
  end

  def profile
    user = current_user
    @methods = user.ruby_methods.reverse.drop(1)
    @methods = user.generate_new_ruby_method if @methods.count == 0
    @method = user.get_latest_method
    render 'profile'
  end

  def destroy
    id = params[:id]
    if can_destroy_user? id
      user = User.find(id).destroy
      flash[:success] = t('user_destroyed', user_name: user.name)
    else
      flash[:danger] = t('cant_delete_yourself')
    end
    redirect_to users_path
  end

  def update
    id = params[:user_id]
    @user = User.find(id)

    if !@user.is_admin? && @user.update_attributes(is_activated: params[:completed])
      @message = {message: t('user_updated', user_name: @user.name), button: 'OK'}
    else
      @message = {message: t('cant_deactivate_admin'), button: 'OK'}
    end
    render 'shared/_snackbar'
  end

  private

  # Defines, whether the user is allowed to destroy other users (excluding himself)
  def can_destroy_user?(id)
    params_id = id.to_i
    current_id = current_user.id

    current_user.is_admin? && params_id != current_id
  end

  # redirects to locker account page if user is not activated
  def is_activated?
    redirect_to account_locked_path if user_signed_in? && !current_user.is_activated?
  end
end
