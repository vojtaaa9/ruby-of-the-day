class RubyMethod < ApplicationRecord
  has_many :ruby_aliases
  has_many :ruby_calls
  has_many :users_ruby_methods
  has_many :users, through: :users_ruby_methods

  # Return date, when the method was created for particular User
  def get_created_date(user_id)
    users_ruby_methods.find_by(user_id: user_id).created_at
  end
end
