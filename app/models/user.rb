class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :validatable, :confirmable
  has_many :users_ruby_methods
  has_many :ruby_methods, through: :users_ruby_methods

  def generate_new_ruby_method
    generated_method = RubyMethod.offset(rand(RubyMethod.count)).first
    if ruby_methods.include?(generated_method)
      generate_new_ruby_method
    else
      ruby_methods << generated_method
    end
  end

  def get_latest_method
    latest_method = users_ruby_methods.sort_by(&:created_at).reverse.first
    RubyMethod.find(latest_method.ruby_method_id)
  end
end
