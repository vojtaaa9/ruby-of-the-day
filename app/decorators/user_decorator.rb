class UserDecorator < ApplicationDecorator
  delegate_all

  # Defines Collection decorator class for usage with Kaminari gem (pagination)
  def self.collection_decorator_class
    PaginatingDecorator
  end

  def shortened_name
    "#{model.name} #{model.last_name.first}."
  end

end
