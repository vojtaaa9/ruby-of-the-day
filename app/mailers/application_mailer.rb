class ApplicationMailer < ActionMailer::Base
  layout 'mailer'
  helper :mailer
end
