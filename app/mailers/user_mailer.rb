class UserMailer < ApplicationMailer
  default from: 'noreply@rubyoftheday.de'

  def ruby_method_of_the_day(user)
    @user = user
    @method = user.get_latest_method
    mail(to: @user.email, subject: 'Ruby of the Day!')
  end
end
