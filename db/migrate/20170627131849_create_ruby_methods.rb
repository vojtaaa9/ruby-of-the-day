class CreateRubyMethods < ActiveRecord::Migration[5.1]
  def change
    create_table :ruby_methods do |t|
      t.text :name
      t.text :identifier
      t.text :type_obj
      t.text :ruby_class
      t.text :description
      t.text :detail
      t.text :url

      t.timestamps
    end
  end
end
