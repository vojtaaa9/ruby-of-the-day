class CreateRubyAliases < ActiveRecord::Migration[5.1]
  def change
    create_table :ruby_aliases do |t|
      t.text :name
      t.belongs_to :ruby_method, index: true

      t.timestamps
    end
  end
end
