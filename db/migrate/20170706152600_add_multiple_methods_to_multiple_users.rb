class AddMultipleMethodsToMultipleUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users_ruby_methods, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :ruby_method, index: true
      t.timestamps
    end
  end
end
