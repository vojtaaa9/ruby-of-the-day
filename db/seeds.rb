# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
require 'json'

RubyMethod.transaction do
  User.create!(title: 'Mr. ÜberGEEK',
               name: 'Vojta',
               last_name: 'Pesek',
               email: 'pesek@webit.de',
               password: '12345678',
               password_confirmation: '12345678',
               is_admin: true,
               confirmed_at: Time.zone.now)

  User.create!(name: 'Non-admin',
               last_name: 'user',
               email: 'nonadmin@example.com',
               password: '12345678',
               password_confirmation: '12345678',
               is_admin: false,
               confirmed_at: Time.zone.now)

  User.create!(name: 'Other admin',
               last_name: 'User',
               email: 'admin@example.com',
               password: '12345678',
               password_confirmation: '12345678',
               is_admin: true,
               confirmed_at: Time.zone.now)

  97.times do
    name  = Faker::Pokemon.unique.name
    last_name = Faker::Ancient.titan
    email = Faker::Internet.email name
    password = Faker::Internet.password(6, 20)
    User.create!(name:  name,
                 last_name: last_name,
                 email: email,
                 password: password,
                 password_confirmation: password,
                 confirmed_at: Time.zone.now)
  end
end

file = File.read('db/rubyCore.json')
data_hash = JSON.parse(file)

RubyMethod.transaction do
  data_hash['methods'].each do |method|
    ruby_method = RubyMethod.new(name: method['name'],
                                 identifier: method['identifier'],
                                 type_obj: method['type'],
                                 ruby_class: method['class'],
                                 description: method['description'],
                                 detail: method['detail'],
                                 url: method['url'])

    method['calls'].each do |call|
      call = RubyCall.new(name: call)
      ruby_method.ruby_calls << call
    end

    method['alias']['methods'].each do |aliass|
      ruby_alias = RubyAlias.new( name: aliass['method'])
      ruby_method.ruby_aliases << ruby_alias
    end

    ruby_method.save!
  end
end

