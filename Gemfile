source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.1'
# Use sqlite3 as the database for Active Record
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'bcrypt', '3.1.11'
gem 'bootstrap-sass', '3.3.7'
gem 'devise', '~> 4.3'
gem 'draper', '~> 3.0'
gem 'slim-rails'
gem 'jquery-rails', '4.3.1'
gem 'jquery-turbolinks'
gem 'simple_form', '~> 3.5'
gem 'trailblazer', '~> 2.0', '>= 2.0.6'
gem 'uglifier', '>= 1.3.0'
gem 'json'
gem 'html2slim'
gem 'kaminari'
gem 'cronic'
gem 'rails-i18n', '~> 5.0', '>= 5.0.4'
gem 'route_translator'
gem 'devise-i18n'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'rails_real_favicon'
  gem 'faker', '~> 1.4', '>= 1.4.3'
end

group :test do
  gem 'guard', '2.13.0'
  gem 'guard-minitest', '2.4.4'
  gem 'minitest'
  gem 'minitest-reporters', '1.1.14'
  gem 'rails-controller-testing', '1.0.2'
  gem 'capybara', '~> 2.13'
  gem 'capybara-screenshot', '~> 1.0', '>= 1.0.14'
  gem 'poltergeist', '~>1.6.0'
  gem 'phantomjs', '1.9.7.1', require: 'phantomjs/poltergeist'
end