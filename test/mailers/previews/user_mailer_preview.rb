# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def ruby_method_of_the_day
    UserMailer.ruby_method_of_the_day(User.first)
  end
end
