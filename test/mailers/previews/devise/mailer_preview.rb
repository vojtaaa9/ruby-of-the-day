class Devise::MailerPreview < ActionMailer::Preview
  # hit http://localhost:3000/rails/mailers/devise/mailers for all devise related mailers
  def confirmation_instructions
    Devise::Mailer.confirmation_instructions(User.first, {})
  end

  def reset_password_instructions
    Devise::Mailer.reset_password_instructions(User.first, {})
  end

  def password_change
    Devise::Mailer.password_change(User.first, {})
  end

  def email_changed
    Devise::Mailer.email_changed(User.first, {})
  end
end