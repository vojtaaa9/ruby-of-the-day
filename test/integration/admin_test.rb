require 'test_helper'

class AdminTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:admin)
    self.default_url_options = { locale: I18n.default_locale }
  end

  test 'should see list of users when logged in as admin' do
    visit('/login')
    within('#new_user') do
      fill_in('E-Mail', with: @admin.email)
      fill_in('Passwort', with: '12345678')
    end

    click_button 'Anmelden'
    page.assert_current_path user_profile_path(user_id: @admin.id)
    assert has_content? 'Erfolgreich angemeldet.'
    assert has_content? @admin.email
    assert has_css? "a[href='/users']"
    assert has_content? "Ruby Methode für #{I18n.l Time.current.to_date}:"

    click_link 'Benutzer'

    assert has_css? 'table.table tbody tr', count: 25
    assert has_css? 'ul.pagination'
  end

  test 'should change user count after delete' do
    login_as @admin
    visit('/users')

    assert_difference 'User.count', -1 do
      find_link('löschen', href: '/delete_user?id=31').click
    end

    assert has_content? 'User 31 wurde gelöscht.'
    within 'table.table' do
      assert has_no_content? 'User 31'
    end
    click_link '2'
    assert has_css? 'table.table tbody tr', count: 6
  end

  test 'user admin cant see delete link and input Activated? is disabled for other admins' do
    login_as @admin
    visit('/users')

    page.assert_current_path users_path

    assert has_no_css? "a[href='#{delete_user_path(id: @admin.id)}']"
    assert has_css? "input[type='checkbox'][checked][disabled][id='#{@admin.id}']"
  end

end
