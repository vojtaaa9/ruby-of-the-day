require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:admin)
    @nonactivated_user = users(:nonactivated_user)
  end

  test 'Sign up with correct credentials' do
    visit('/register')
    within('#new_user') do
      fill_in('Name', with: 'John')
      fill_in('Last name', with: 'Doe')
      fill_in('E-Mail', with: 'john@email.com')
      fill_in('user_password', with: '12345678')
      fill_in('user_password_confirmation', with: '12345678')
    end
    click_button 'Registrieren'
    assert has_content?('Sie erhalten in wenigen Minuten eine E-Mail mit einem Link für die Bestätigung der Registrierung. Klicken Sie auf den Link um Ihren Account zu aktivieren.')
  end

  test 'Sing up with incorrect credentials' do
    visit('/register')
    within('#new_user') do
      fill_in('Name', with: 'd')
      fill_in('Last name', with: 's')
      fill_in('E-Mail', with: 'john.m@sdvf.cs')
      fill_in('user_password', with: '1234')
      fill_in('user_password_confirmation', with: '12378')
    end
    click_button 'Registrieren'
    assert has_content?('Please review the problems below:')
  end

  test 'Login with correct credentials' do
    visit('/login')
    within('#new_user') do
      fill_in('E-Mail', with: @admin.email)
      fill_in('Passwort', with: '12345678')
    end
    click_button 'Anmelden'
    assert has_content?('Erfolgreich angemeldet.')

    # logout link is hidden (display: none) --> capybara can't click on it
    click_link @admin.email
    click_link 'Abmelden'
    assert has_content?('Erfolgreich abgemeldet.')
  end

  test 'Login with incorrect credentials' do
    visit('/login')

    within('#new_user') do
      fill_in('E-Mail', with: @admin.email)
      fill_in('Passwort', with: '')
    end
    click_button 'Anmelden'
    assert has_content?('Ungültige Anmeldedaten.')

    within('#new_user') do
      fill_in('E-Mail', with: '')
      fill_in('Passwort', with: '12345678')
    end
    click_button 'Anmelden'
    assert has_content?('Ungültige Anmeldedaten.')
  end

  test 'User cant use account as nonactivated user' do
    visit('/login')

    within('#new_user') do
      fill_in('E-Mail', with: @nonactivated_user.email)
      fill_in('Passwort', with: '12345678')
    end
    click_button 'Anmelden'

    assert has_content? 'Erfolgreich angemeldet.'

    page.assert_current_path account_locked_path
    assert has_content? 'Dein Account ist gesperrt!'
  end

  test 'Cancel user account' do
    login_as @admin

    visit('/')
    click_link @admin.email
    click_link 'Profil ändern'

    page.assert_current_path edit_user_registration_path

    click_link 'Konto löschen'

    page.assert_current_path root_path
    assert has_content? 'Ihr Account wurde gelöscht.'
  end
end