require 'test_helper'

class SendmailRubymethodJobTest < ActiveJob::TestCase

  def setup
    @nonconfirmed_user = users(:nonconfirmed_user)
    @nonactivated_user = users(:nonactivated_user)
  end

  test 'should send email only to active users with confirmed mail' do
    users = SendmailRubymethodJob.run
    assert_not users.include? @nonactivated_user_user
    assert_not users.include? @nonconfirmed_user
  end
end
