require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:admin)
    @vojta = users(:vojta)
    @non_admin = users(:user_0)
  end

  test 'should get home' do
    get root_path
    assert_response :success
  end

  test 'should redirect when not logged in and deleting user' do
    assert_no_difference 'User.count' do
      delete delete_user_path(id: @vojta.id)
      assert_redirected_to new_user_session_path
    end
  end

  test 'should redirect when logged in as non admin and deleting user' do
    login_as @non_admin
    assert_no_difference 'User.count' do
      delete delete_user_path(id: @vojta.id)
    end
    assert_redirected_to users_path
  end

  test 'should delete when logged in as admin and deleting other user' do
    login_as @admin

    assert_difference 'User.count', -1 do
      delete delete_user_path(id: @vojta.id)
    end
    assert_redirected_to users_path
    follow_redirect!
    assert_select 'div.alert-success', text: "×#{@vojta.name} wurde gelöscht."
  end

  test 'should redirect when deleting yourself' do
    login_as @admin

    assert_no_difference 'User.count' do
      delete delete_user_path(id: @admin.id)
    end

    assert_redirected_to users_path
    follow_redirect!
    assert_select 'div.alert-danger', text: "×Du kannst dein eigenes Account nicht löschen. Falls du deine Mitgliedschaft kündigen willst, gehe in deinen Account und klick auf Konto löschen."
  end

  test 'should redirect when deactivating yourself' do
    login_as @admin

    assert_no_difference 'User.count' do
      post user_update_path(user_id: @vojta.id)
    end

    assert_template 'shared/_snackbar'
    assert_select 'div.snackbar-message', text: 'Du kannst admins nicht deaktivieren!'
  end
end
