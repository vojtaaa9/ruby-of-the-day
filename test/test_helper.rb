require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'capybara/rails'
require 'capybara/minitest'
require 'capybara/minitest/spec'
require 'capybara/poltergeist'
require 'capybara-screenshot/minitest'
require 'minitest/reporters'
MiniTest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  include Capybara::Minitest::Assertions
  include Capybara::Screenshot::MiniTestPlugin
  include Devise::Test::IntegrationHelpers

  # Don't ask me why this is needed with transactional fixtures
  # but it is!
  setup do
    Capybara.reset!

    # Keep only the screenshots generated from the last failing test suite
    Capybara::Screenshot.prune_strategy = :keep_last_run
  end

end

class ActionDispatch::IntegrationTest
  Capybara.default_driver = :poltergeist
  Capybara.asset_host = 'http://localhost:3000'

end