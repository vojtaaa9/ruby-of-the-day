# To start: script/cronic -d -l log/cronic.log -P tmp/pids/cronic.pid
# To stop: script/cronic -k -P tmp/pids/cronic.pid

# run every day at 8 o clock
cron '0 8 * * *' do
  SendmailRubymethodJob.run
end

