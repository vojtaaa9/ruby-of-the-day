Rails.application.routes.draw do

  localized do
    root 'users#home'
    delete 'delete_user', to: 'users#destroy'
    get 'account_locked', to: 'users#account_locked'

    devise_for :users, path: '', path_names:
    { sign_in: 'login',
    sign_out: 'logout',
    sign_up: 'register',
    edit: 'settings' }

    resources :users do
      post 'update'
      get 'profile'
    end
  end

  get '/de', to: 'users#home'
end